#ifndef TIMING_H
#define TIMING_H

#include <stdbool.h>
#include <sys/time.h>

long timeval_to_long(struct timeval time);

void print_time(long tv, bool parse_mode);

void* timer(void* arg_ptr);

#endif // TIMING_H
